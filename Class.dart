void main(List<String> args) {}

class Apps {
  final String appName;
  final String appCat;
  final String appDeveloper;
  final String appWinYear;
  Apps({
    required this.appName,
    required this.appCat,
    required this.appDeveloper,
    required this.appWinYear,
  });

  void capitaliseApp() {
    print(appName.toUpperCase());
  }
}
